/*  MAX7221 library for Arduino
 Copyright (C) 2009 David Pye    <davidmpye@gmail.com
 Copyright (C) 2012 Kasper Skårhøj <kasperskaarhoj@gmail.com>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "MAX7221.h"
#include "SPI.h"


MAX7221::MAX7221() :
	slaveSelectPin(0){
}

MAX7221::MAX7221(int SS_pin) :
		slaveSelectPin(0){
	begin( SS_pin);
}

void MAX7221::begin(int SS_pin) {
	slaveSelectPin= SS_pin;
	SPI.begin();
}

bool MAX7221::init() {
	// If this value is true (return value of this function), we assume the board actually responded and is "online"
	bool retVal = false;

	return retVal;
}
