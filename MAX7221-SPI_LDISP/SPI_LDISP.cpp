/*
 * SPI_LDISP.cpp
 *
 *  Created on: Mar 26, 2014
 *      Author: frits
 */

#include "SPI_LDISP.h"

spi_ldisp_io::spi_ldisp_io() :
		devicesCount(0){
	for (int i = 0; i < MAXDEVICES; i++){
		devices[i] = NULL;
	}
}

spi_ldisp_io::~spi_ldisp_io() {
	for (int i = 0; i < MAXDEVICES; i++)
		if (devices[i] != NULL)
			delete (devices[i]);
}

MAX7221* spi_ldisp_io::newDevices(int SS_pin) {
	if (!exists( SS_pin))
	{
		if (++devicesCount < MAXDEVICES)
		{
			devices[devicesCount]=new MAX7221(SS_pin);
		}
		else
		{
			return NULL;
		}
	}
	return devices[devicesCount];
}

bool spi_ldisp_io::exists(int SS_pin) {
	for (int i = 0; i < MAXDEVICES; i++){
		if (devices[i] != NULL){
			if( devices[i]->getSlaveSelectPin()==SS_pin)
				return true;
		}
	}
	return false;
}

word spi_ldisp_io::parseCommand(SDTP cmdLine) {

	MAX7221 *newDevPtr;
	word t,value = 0;
	char devIndex = cmdLine.subAddress;
	if (!exists(cmdLine.subAddress))
		if (newDevices(devIndex) == NULL)
			return false;
	switch (cmdLine.cmd) {
	case RESET:
		break;
	case INIT:	// Test OK

		break;
	case SYNC:
		break;
	case SETVAL: // Test OK
		break;
	case SETVALALL: // Test OK
		value = *((word*) cmdLine.data);
		break;
	case REQVAL: // Test OK
		Serial.println("REQVAL");
		break;
	case REQVALALL: // Test OK
		break;
	case REGISTER:
		break;
	default:
		break;
	}
	return value;
}

void spi_ldisp_io::interruptHandler(void) {

}

