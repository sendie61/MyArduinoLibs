/*
 * SPI_LDISP.h
 *
 *  Created on: Mar 26, 2014
 *      Author: frits
 */

#ifndef SPI_LDISP_H_
#define SPI_LDISP_H_

#include <MAX7221.h>
#include <SDTProtocol.h>

#define MAXDEVICES	8

class spi_ldisp_io {
public:
	spi_ldisp_io();
	virtual ~spi_ldisp_io();
	// TODO newDevices
	MAX7221* newDevices(int SS_pin);
	// TODO Test exists!
	bool exists(int SS_pin);
	// TODO parseCommand
	word parseCommand(SDTP cmdLine);
	void interruptHandler(void);

private:
	SDTProtocol proc;
	MAX7221* devices[MAXDEVICES];	// max. nr of MAX7221 deviceStrings on the SPI bus is MAXDEVICES
	byte devicesCount;				// how many objects do exist
};

#endif /* SPI_LDISP_H_ */
