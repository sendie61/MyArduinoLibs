/*
 * I2CIO.h
 *
 *  Created on: Mar 26, 2014
 *      Author: frits
 */

#ifndef I2CIO_H_
#define I2CIO_H_

#include <MCP23017.h>
#include <SDTProtocol.h>

static unsigned char Bit2Pin[] = { 8, 9, 10, 11, 12, 13, 14, 15, 0, 1, 2, 3, 4,
		5, 6, 7 };

class i2c_io {
public:
	i2c_io();
	virtual ~i2c_io();
	// Tested newDevice OK!
	MCP23017* newDevice(int I2CAddr);
	// TODO Test exists!
	bool exists(int I2CAddr);
	// Tested parseCommand OK!
	word parseCommand(SDTP cmdLine);
	void interruptHandler(void);

private:
	SDTProtocol proc;
	MCP23017* device[8];		// max. nr of MCP23017 devices on 1 I2C bus is 8
	byte deviceCount;			// how many objects do exist
};

#endif /* I2CIO_H_ */
