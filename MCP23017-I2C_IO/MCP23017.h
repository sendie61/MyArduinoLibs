/*  MCP23017 library for Arduino
 Copyright (C) 2009 David Pye    <davidmpye@gmail.com>
 Copyright (C) 2012 Kasper Skårhøj <kasperskaarhoj@gmail.com>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MCP23017_H
#define MCP23017_H

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

/**
 Version 1.0.0
 (Semantic Versioning)
 **/

// IMPORTANT on byte order:
// Note on Writing words: The MSB is for GPA7-0 and the LSB is for GPB7-0
// Pinnumbers 0-7 = GPB0-7, 8-15 = GPA0-7
// This comes across as slightly un-intuitive when programming
#include "Wire.h"

//Register defines from data sheet - we set IOCON.BANK to 0
//as it is easier to manage the registers sequentially.
#define MCP23017_IODIR 	 	0x00	// I/O DIRECTION REGISTER
#define MCP23017_IPOL 	 	0x02	// INPUT POLARITY REGISTER
#define MCP23017_GPINTEN 	0x04 	// INTERRUPT-ON-CHANGE CONTROL REGISTER
#define MCP23017_DEFVAL  	0x06	// DEFAULT VALUE REGISTER FOR INTERRUPT-ON-CHANGE
#define MCP23017_INTCON  	0x08	// INTERRUPT CONTROL REGISTER
#define MCP23017_IOCON		0x0A 	// CONFIGURATION REGISTER
#define MCP23017_GPPU 	 	0x0C	// PULL-UP RESISTOR CONFIGURATION REGISTER
#define MCP23017_INTF 	 	0x0E	// INTERRUPT FLAG REGISTER
#define MCP23017_INTCAP 	0x10	// INTERRUPT CAPTURE REGISTER
#define MCP23017_GPIO 	 	0x12	// PORT REGISTER
#define MCP23017_OLAT 	 	0x14	// OUTPUT LATCH REGISTER

#define MCP23017_I2C_BASE_ADDRESS 0x40

class MCP23017 {
public:
	//NB the i2c address here is the value of the A0, A1, A2 pins ONLY
	//as the class takes care of its internal base address.
	//so i2cAddress should be between 0 and 7
	MCP23017();
	void begin(int i2cAddress);
	bool init();

	//These functions provide an 'arduino'-like functionality for accessing
	//pin states/pullups etc.
	// TODO Test pinMode!
	void pinMode(int pin, int mode);
	// Tested digitalWrite OK!
	void digitalWrite(int pin, int val);
	// Tested digitalRead OK!
	int digitalRead(int pin);

	//These provide a more advanced mapping of the chip functionality
	//See the data sheet for more information on what they do

	//Returns a word with the current pin states (ie contents of the GPIO register)
	// Test getGPIO OK!
	word getGPIO();

	//Allows you to write a word to the GPIO register
	// Tested setGPIO OK!
	void setGPIO(word w);

	//Returns a word with the current pin states (ie contents of the GPIO register)
	// Tested getINTF OK!
	word getINTF();

	//Returns a word with the current pin states (ie contents of the GPIO register)
	// Tested getINTCAP OK!
	word getINTCAP();

	//Returns a word with the current pin states (ie contents of the GPIO register)
	// Tested getOLAT OK!
	word getOLAT();

	//Sets up the polarity mask that the MCP23017 supports
	//if set to 1, it will flip the actual pin value.
	// Tested setIPOL OK!
	void setIPOL(word mask);

	// Tested setGPINTEN OK!
	void setGPINTEN(word mask);

	// Test setDEFVAL OK!
	void setDEFVAL(word mask);

	// Tested setINTCON OK!
	void setINTCON(word mask);

	//Sets which pins are inputs or outputs (1 = input, 0 = output) NB Opposite to arduino's
	//definition for these
	// Tested setIODIR OK!
	void setIODIR(word mask);

	//Allows enabling of the internal 100k pullup resisters (1 = enabled, 0 = disabled)
	// Tested setGPPU OK!
	void setGPPU(word mask);

	//Allows change settings of the I/O EXPANDER CONFIGURATION REGISTER
	// Tested setIOCON OK!
	void setIOCON(word mask);

	//Interrupt functionality
	// returns 0 if no more pending interrupts
	// TODO Test interruptHandler
	word interruptHandler( void);

private:
	void writeRegister(int regaddress, byte val);
	// tested OK
	void writeRegister(int regaddress, word val);
	word readRegister(int regaddress);

	//Our actual i2c address
	byte _i2cAddress;

	//Cached copies of the register vales
	word _IODIR, _IPOL, _GPINTEN, _DEFVAL, _INTCON, _GPPU, _GPIO, _INTF,
			_INTCAP, _OLAT;
};
#endif 
