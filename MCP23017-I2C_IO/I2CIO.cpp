/*
 * I2CIO.cpp
 *
 *  Created on: Mar 26, 2014
 *      Author: frits
 */

#include "I2CIO.h"

i2c_io::i2c_io() :
		deviceCount(0){
	for (int i = 0; i < 8; i++){
		device[i] = NULL;
	}
}

i2c_io::~i2c_io() {
	for (int i = 0; i < 8; i++)
		if (device[i] != NULL)
			delete (device[i]);
}

MCP23017* i2c_io::newDevice(int I2CAddr) {
	if (!exists(I2CAddr)) {
		if ((device[I2CAddr] = new MCP23017) == NULL) {
			return NULL;
		}
		device[I2CAddr]->begin(I2CAddr);
		device[I2CAddr]->init();
		deviceCount++;
	}
	return device[I2CAddr];
}

bool i2c_io::exists(int I2CAddr) {
	return (device[I2CAddr] != NULL);
}

word i2c_io::parseCommand(SDTP cmdLine) {

	MCP23017 *newDevPtr;
	word t,value = 0;
	char devIndex = cmdLine.subAddress;
	if (!exists(cmdLine.subAddress))
		if (newDevice(devIndex) == NULL)
			return false;
	switch (cmdLine.cmd) {
	case RESET:
		break;
	case INIT:	// Test OK
		device[devIndex]->setIODIR(*((word*) cmdLine.data));
		device[devIndex]->setIPOL(*((word*) cmdLine.data + 1));
		device[devIndex]->setGPINTEN(*((word*) cmdLine.data + 2));
		device[devIndex]->setDEFVAL(*((word*) cmdLine.data + 3));
		device[devIndex]->setINTCON(*((word*) cmdLine.data + 4));
		device[devIndex]->setGPPU(*((word*) cmdLine.data + 5));
		device[devIndex]->getGPIO();
		break;
	case SYNC:
		break;
	case SETVAL: // Test OK
		device[devIndex]->digitalWrite(cmdLine.data[0], cmdLine.data[1]);
		break;
	case SETVALALL: // Test OK
		value = *((word*) cmdLine.data);
		device[devIndex]->setGPIO(value);
		break;
	case REQVAL: // Test OK
		Serial.println("REQVAL");
		value= device[devIndex]->digitalRead(cmdLine.data[0]);

		proc.assembleCommand( SETVAL, 0, 2, (byte) cmdLine.data[0], (byte) value);
		Serial.println("REQVAL2");
		break;
	case REQVALALL: // Test OK
		value= device[devIndex]->getGPIO();
		break;
	case REGISTER:
		break;
	default:
		break;
	}
	return value;
}

void i2c_io::interruptHandler(void) {
	byte subAddress = 0;
	byte tries = 0;
	word flags = 0;
	while (tries < deviceCount) {
		if (exists(subAddress)) {
			if ((flags = device[subAddress]->getINTF()) != 0) {	// did we find something?
				word captured = device[subAddress]->getINTCAP();
				Serial.println("Found Interrupt source!!");
				device[subAddress]->getGPIO();
				//handle the inputs here!
			}
			tries++;
		}
	}
}

